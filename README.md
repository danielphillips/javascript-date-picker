Be sure to include the DJP.js library, found in my repositories.

Here is the basic usage. 

The dp_container class enables an element to act as a container for the calendar, there are 4 inputs, three are hidden which will store the date values, one is visible which will display the selected date. 

The script scans the document for any elements of class .dp, this will trigger the date picker to be enabled around it and it's parent container.

```
<form method="post">

	<table>
        <tr>
        	<td>From:</td>
        	<td><input class="dp" type="text" name="from_date" value="" ></td>
        </tr>
        <tr>
        	<td>To:</td>
        	<td><input class="dp" type="text" name="to_date" value="" ></td>
        </tr>
    </table>

</form>
```

The form elements posted are as follows:

```
$_METHOD['day{main_input_name}'];
$_METHOD['month{main_input_name}'];
$_METHOD['year{main_input_name}'];
```

For our example above that would be:

```
$_POST['dayfrom_date'];
$_POST['monthmainfrom_date'];
$_POST['yearfrom_date'];

$_POST['dayto_date'];
$_POST['monthto_date'];
$_POST['yearto_date'];
```
If there is no name attribute on the main input on your page, the posted data will be named 'day, 'month' and 'year'
