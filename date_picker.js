(function(){
    if(!window.DJP) return;
    if(!window.DatePicker) window['DatePicker'] = {};
    
	var block_past_identifier 	= 'dp_block_past';
	var fast_jump_identifier 	= 'dp_jump_year';
	var lang 					= 'en';
	var year_range_jumapable	= new Array('1900',new Date().getFullYear() + 10);
	var year_range_jumapable_block_past	= new Array(new Date().getFullYear(), new Date().getFullYear() + 10);
	var today_date				= getNow();
    
	var cals_inputs				= new Array(); // holds each date input element
	var cals					= new Array(); // holds each date input element
    
	var months					= new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
	var days					= new Array("Mon","Tue","Wed","Thu","Fri","Sat","Sun");
    
	var past_date_prohibited	= 'Past date not allowed';
	var close_link_text			= 'close';
    
    function init(){
		switch(lang){
			case 'fr':
				months					= new Array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre");
				days					= new Array("Lun","Mar","Mer","Jeu","Ven","Sam","Dim");
				past_date_prohibited	= 'Date antérieur interdit';
				close_link_text			= 'fermer';
				break;
			case 'es':
				months					= new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
				days					= new Array("Lun","Mar","Mié","Jue","Vie","Sáb","Dom");
				past_date_prohibited	= 'Fecha antecedentes prohibido';
				close_link_text			= 'cerca';
				break;
			case 'de':
				months					= new Array("Juanuar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember");
				days					= new Array("Mon","Die","Mit","Don","Fre","Sam","Son");
				past_date_prohibited	= 'Past Datum verboten';
				close_link_text			= 'enge';
				break;
			case 'bg':
				months					= new Array("Януари", "Февруари", "Март", "Април", "Май", "Юни", "Юли", "Август", "Септември", "Октомври", "Ноември", "Декември");
				days					= new Array("Mon","Die","Mit","Don","Fre","Sam","Son");
				past_date_prohibited	= 'Минали дата забранено';
				close_link_text			= 'близо';
				break;
		}
		
        var tmp_cals_inputs = DJP.getElementsByClassName('dp');
        for(var i = 0; i< tmp_cals_inputs.length; i++){
			var main_input = tmp_cals_inputs[i].cloneNode();

			main_input.setAttribute('readonly', 'readonly');
			
			var main_name = main_input.getAttribute('name') ? main_input.getAttribute('name') : '';
			
			var date_container 	= document.createElement('div');
			DJP.addClass(date_container, 'dp_container');
			

			var parts = main_input.getAttribute('value').split('/');
			var d = parts[0];
			var m = parts[1];
			var y = parts[2];
			
			var input_day 		= document.createElement('input');
			input_day.setAttribute('type', 'hidden');
			input_day.setAttribute('name', 'day'+main_name);
			input_day.setAttribute('class', 'day');
			input_day.setAttribute('value', d);
			var input_month 	= input_day.cloneNode();
			input_month.setAttribute('name', 'month'+main_name);
			input_month.setAttribute('class', 'month');
			input_month.setAttribute('value', m);
			var input_year 		= input_day.cloneNode();
			input_year.setAttribute('name', 'year'+main_name);
			input_year.setAttribute('class', 'year');
			input_year.setAttribute('value', y);
			
			date_container.appendChild(input_day);
			date_container.appendChild(input_month);
			date_container.appendChild(input_year);
			date_container.appendChild(main_input);
			
			var jump = DJP.hasClass(main_input, fast_jump_identifier);
			if(jump){
				
				DJP.addClass(date_container, fast_jump_identifier);
				DJP.removeClass(main_input, fast_jump_identifier)
				
			}
			var block = DJP.hasClass(main_input, block_past_identifier);
			if(block){
				
				DJP.addClass(date_container, block_past_identifier);
				DJP.removeClass(main_input, block_past_identifier)
				
			}
			
			tmp_cals_inputs[i].parentNode.insertBefore(date_container, tmp_cals_inputs[i]);

			DJP.removeElement(tmp_cals_inputs[i]);

			DJP.addEvent(main_input, DatePicker.focused, 'focus');
		
			cals_inputs.push(main_input);
        }
		
    }
    window['DatePicker']['init'] = init;
    
    function getSelected(_this){
        var d = DJP.getElementsByClassName('day','input',_this.parentNode)[0].value;
        var m = DJP.getElementsByClassName('month','input',_this.parentNode)[0].value;
        var y = DJP.getElementsByClassName('year','input',_this.parentNode)[0].value;
        if(!d || !m || !y){
            var now = getNow();
        }else{
            var now = {'day':d, 'month':m, 'year':y};
        }
        return now;
    }
    
	function clearAllCals(){
        var cals = DJP.getElementsByClassName('djp_datepicker','div');
		for(var i = 0 ; i < cals.length ; i++){   
			clear(cals[i]);
		}

	}
	
	
    function focused(e){
		clearAllCals();
        var _this = DJP.getThis(e);
        if(c = getCal(_this)){
            if(c.parentNode.getElementsByTagName('div').length > 0){
                return;
            }else{
                var now = getSelected(_this);

                var div = document.createElement('div');
                DJP.addClass(div, 'djp_datepicker');
                
				/* header (date and navigation arrows */
                var top_header = document.createElement('div');
                
                var left_arrow_div = document.createElement('div');
                DJP.addClass(left_arrow_div,'left_arrow');
                DJP.addClass(left_arrow_div,'arrow');
                DJP.addEvent(left_arrow_div, DatePicker.previous, 'click');
                top_header.appendChild(left_arrow_div);
                
                var right_arrow_div = document.createElement('div');
                DJP.addClass(right_arrow_div,'right_arrow');
                DJP.addClass(right_arrow_div,'arrow');
                DJP.addEvent(right_arrow_div,DatePicker.next,'click');
                top_header.appendChild(right_arrow_div);

				top_header.appendChild(makeTopOfTable(now, DJP.hasClass(_this.parentNode, fast_jump_identifier), DJP.hasClass(_this.parentNode, block_past_identifier) ) ); // month text & drop down year
                div.appendChild(top_header);
				
				/* calendar table */
                var cal_view = makeMonth(now.month,now.year,now.day);
                div.appendChild(cal_view);
                
				
				/* close link */
				var a = document.createElement('a');
				DJP.addClass(a, 'close_link');
				a.appendChild(document.createTextNode(close_link_text));
                DJP.addEvent(a,clearAllCals,'click');
                div.appendChild(a);
				
                c.parentNode.appendChild(div,c);
				
            }
        }
    }
    window['DatePicker']['focused'] = focused;
    
    function removeCal(e){
        var _this = DJP.getThis(e);
        if(c = getCal(_this)){
            setTimeout(function(){clear(c.parentNode.getElementsByTagName('div')[0])},500);
        }
    }
    window['DatePicker']['removeCal'] = removeCal;
    
    function clear(elem){
        var c = elem;
        DJP.removeElement(c.parentNode.getElementsByTagName('div')[0]);
    }
    window['DatePicker']['clear'] = clear;
    
	function jumpToYear(e){
        var _this = DJP.getThis(e); // link
		
		var top			= _this.parentNode.parentNode;
		var container 	= top.parentNode;
        
		var cal = container.getElementsByTagName('table')[0];
        var thisMonth = cal.getAttribute('summary');
		DJP.removeElement(cal);

		
        var m = thisMonth.split('-')[0];
        var y = thisMonth.split('-')[1];

        var now = getSelected(container.parentNode);
        		
		var dropDown = top.getElementsByTagName('select')[0];
		var jump_to_year = dropDown.options[dropDown.selectedIndex].value;
		
        var newView = getNextMonth(m-1, jump_to_year);

        
        cal = makeMonth(newView.month,newView.year);
        container.insertBefore(cal, container.getElementsByTagName('a')[0]);
        
        var text = document.createTextNode(months[newView.month - 1]+' '+newView.year);
		
        var textHolder = top.getElementsByTagName('span')[0];
		textHolder.parentNode.insertBefore(makeTopOfTable({day:1, month:newView.month, year:newView.year}, DJP.hasClass(container.parentNode, fast_jump_identifier), DJP.hasClass(container.parentNode, block_past_identifier) ));
      	DJP.removeElement(top.getElementsByTagName('span')[0]);
       
        cal.setAttribute('summary', newView.month+'-'+newView.year);
	}
	
	function makeTopOfTable(now, fast_change_year, block_past){
		if(fast_change_year){
			var span = document.createElement('span');
			span.appendChild(document.createTextNode(months[now.month - 1]+' ')); // month year
			
			var select_form_element = document.createElement('select');
			var range = block_past ? year_range_jumapable_block_past : year_range_jumapable;
			for(var i = range[0]; i<= range[1]; i++){
				var option_form_element = document.createElement('option');
				option_form_element.appendChild(document.createTextNode(i));
				if(now.year == i){
					option_form_element.setAttribute('selected', 'selected');
				}
					option_form_element.setAttribute('value', i);
				select_form_element.appendChild(option_form_element);
			}
			DJP.addEvent(select_form_element, jumpToYear, 'change');
			
			span.appendChild(select_form_element);
		}else{
			var span = document.createElement('span');
			span.appendChild(document.createTextNode(months[now.month - 1]+' '+now.year)); // month year
		}
		return span;
	}
    
    function next(e){
        var _this = DJP.getThis(e); // link
		
		var top			= _this.parentNode;
		var container 	= top.parentNode;
        
		var cal = container.getElementsByTagName('table')[0];
        var thisMonth = cal.getAttribute('summary');
		DJP.removeElement(cal);
        

        var now = getSelected(container);
        var m = thisMonth.split('-')[0];
        var y = thisMonth.split('-')[1];
        
        var newView = getNextMonth(m, y);
        cal = makeMonth(newView.month,newView.year);
        container.insertBefore(cal, container.getElementsByTagName('a')[0]);

        var text = document.createTextNode(months[newView.month - 1]+' '+newView.year);
        
        var textHolder = top.getElementsByTagName('span')[0];
		textHolder.parentNode.insertBefore(makeTopOfTable({month:newView.month, year:newView.year}, DJP.hasClass(container.parentNode, fast_jump_identifier), DJP.hasClass(container.parentNode, block_past_identifier)));
      	DJP.removeElement(top.getElementsByTagName('span')[0]);
            
        cal.setAttribute('summary', newView.month+'-'+newView.year);
        
    }
    window['DatePicker']['next'] = next;

    function previous(e){
        var _this = DJP.getThis(e); // link
		
		var top			= _this.parentNode;
		var container 	= top.parentNode;
        
		var cal = container.getElementsByTagName('table')[0];
        var thisMonth = cal.getAttribute('summary');
		DJP.removeElement(cal);
        
        var now = getSelected(container);
        var m = thisMonth.split('-')[0];
        var y = thisMonth.split('-')[1];
    
       	var newView = getPreviousMonth(m, y);
           
        cal = makeMonth(newView.month,newView.year);
        container.insertBefore(cal, container.getElementsByTagName('a')[0]);
            
        var text = document.createTextNode(months[newView.month - 1]+' '+newView.year);
        
        var textHolder = top.getElementsByTagName('span')[0];
		textHolder.parentNode.insertBefore(makeTopOfTable({month:newView.month, year:newView.year}, DJP.hasClass(container.parentNode, fast_jump_identifier), DJP.hasClass(container.parentNode, block_past_identifier)));
      	DJP.removeElement(top.getElementsByTagName('span')[0]);
            
        cal.setAttribute('summary', newView.month+'-'+newView.year);
        
    }
    window['DatePicker']['previous'] = previous;
    
    function getNextMonth(m,y){
        if(m == 12){
            return {'month':1,'year':(parseInt(y)+1)}
        }else{
            return {'month':(parseInt(m)+1),'year':y}
        }
    }
    
    function getPreviousMonth(m,y){
        if(m == 1){
            return {'month':12,'year':(parseInt(y)-1)}
        }else{
            return {'month':(parseInt(m)-1),'year':y}
        }
    }

    function getCal(elem){
        var i = cals_inputs.inArray(elem);
        if(i){
            return cals_inputs[i];
        }else{
            return false;
        }
    }
    
    function getNow(){
        var today       = new Date(); //new Date(2010,8,9);

        var thisDay     = today.getDate();
        var thisMonth   = today.getMonth();
        var thisYear    = today.getFullYear();
        
        return {'day':thisDay,'month':thisMonth+1,'year':thisYear};

    }
    
    function makeMonth(m, y, d){
        var table = document.createElement('table');
		table.setAttribute('summary', m+'-'+y);
        var tbody = document.createElement('tbody');
        
        var header = document.createElement('tr');
        
        for(var i = 0; i< days.length; i++){
            var col = document.createElement('th');
            col.setAttribute('scope','col');
            col.appendChild(document.createTextNode(days[i]));
            header.appendChild(col);
        }
        
        tbody.appendChild(header);
        var select_day = d;
        
        
        var theFirst = new Date(y, m-1, 1);
        var weekDayStart = theFirst.getDay() == 0 ? 6 : theFirst.getDay() -1;
        // 0=> monday
        var nextMonth = m == 12 ? 0 : m;
        var correctYear = m == 0 ? y-1 : y;
        
        var int_d = new Date(correctYear, nextMonth,1);
        var d = new Date(int_d - 1);
        
        var number_days = d.getDate();
        
        var cells = new Array();
        var d = 0; // day counter
        var u = 0; // start row
        
        var today = new Date();
        
        do{
            var row;
            for(var i = 0; i < days.length; i++){
                if(i == 0 && row){
                    tbody.appendChild(row);
                    row = null;
                }
                if(!row) row = document.createElement('tr');
                
                var td = document.createElement('td');
                
                if(u < weekDayStart){
                    td.appendChild(document.createTextNode(''));
                    u++;
                }else{
                    if(d < number_days){
                        td.appendChild(document.createTextNode(d+1));
                        DJP.addClass(td,'month_day');
                           
						   
						    DJP.addEvent(td,DatePicker.selectMe,'click');
                        
						
						if(today.getDate() == d+1 && today.getMonth()+1 == m && today.getFullYear() == y){
                            DJP.addClass(td,'today');
                        }else{
                            if(d+1 == select_day){
                                DJP.addClass(td,'selected');
                            }
                        }
                        d++;
                    }else{
                        td.appendChild(document.createTextNode(''));
                    }
                }
                
                row.appendChild(td);
                
            }
            
            tbody.appendChild(row);
            
        }while(d < number_days);
        table.appendChild(tbody);
        return table;
    }

    function selectMe(e){
        var _this = DJP.getThis(e); // cell
		var month_table = _this.parentNode.parentNode.parentNode;
        var calDiv = month_table.parentNode; // add extra parentNode to compensate for the tbody element inside the table
        var thisMonth = month_table.getAttribute('summary'); // add extra parentNode to compensate for the tbody element inside the table
        var m = thisMonth.split('-')[0];
        var y = thisMonth.split('-')[1];
		
		if(DJP.hasClass(calDiv.parentNode, block_past_identifier)){
			var d = parseInt(_this.innerHTML);
			if(d < today_date.day && today_date.month == m && today_date.year == y){
				alert(past_date_prohibited);
				return;
			}
		}
						
						
						
        
        var big_container = calDiv.parentNode;
        var inp_test = DJP.getElementsByClassName('dp', 'input',big_container)[0];
        var ipt = getCal(inp_test);
        var container = ipt.parentNode;
        DJP.getElementsByClassName('day','input',container)[0].value = _this.innerHTML;
        DJP.getElementsByClassName('month','input',container)[0].value = m;
        DJP.getElementsByClassName('year','input',container)[0].value = y;
        
        ipt.value = _this.innerHTML+'/'+m+'/'+y;
        
        clear(calDiv);
    }
    window['DatePicker']['selectMe'] = selectMe;
    
 
    
    DJP.addLoadEvent(DatePicker.init());
    
})();
